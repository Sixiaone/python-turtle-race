from turtle import *
from random import randint
import time

def playGame():
    #code to play game
    #draw board, title, and turtles
    ### code to draw the title ###
    franklin=Turtle()
    franklin.color('red')
    franklin.shape('turtle')
    franklin.hideturtle()

    bob=Turtle()
    bob.color('red')
    bob.shape('turtle')
    bob.hideturtle()

    tina=Turtle()
    tina.color('red')
    tina.shape('turtle')
    tina.hideturtle()
    
    drawTitle()
    drawBoard()

    drawTurtles(franklin,bob,tina)

    startRace(franklin,bob,tina)
       

def showMenu():
    print("WELCOME TO TURTLE RACES!")
    print("Grab a friend (or a few) and guess which color turtle will win the race!")
    print("Red = Franklin")
    print("Blue = Bob")
    print("Green = Tina")
    print()
    print("Ready? (press 'enter' to continue)")
    input()

def drawTitle():
    penup()
    goto(-70,190)
    write("Turtle Races", font=("Arial",20,"normal"))
    #code to draw title 
    
def drawBoard():
    drawTitle()
    speed(10)
    penup()
    goto(-140,140)

    for i in range(15):
        write(i)
        right(90)
        forward(10)
        pendown()
        forward(150)
        penup()
        backward(160)
        left(90)
        forward(20)
        
    #code to draw entire game "board"

    
def drawTurtles(franklin, bob, tina):
    franklin.showturtle()
    franklin.penup()
    franklin.goto(-160,100)
    franklin.pendown()

    bob.showturtle()
    bob.penup()
    bob.goto(-160,50)
    bob.pendown()
    
    tina.showturtle()
    tina.penup()
    tina.goto(-160,0)
    tina.pendown()
    #code to draw turtles

    

def startRace(franklin, bob, tina):
    #code to do all of the race
    for turn in range(200):
        if franklin.xcor>=140:
            penup()
            goto(-90,150)
            color("red")
            write("Franklin wins")
            break
        elif bob.xcor>=140:
            penup()
            goto(-90,150)
            color("blue")
            write("Bob wins")        
            break
        elif tina.xcor>=140:
            penup()
            goto(-90,150)
            color("green")
            write("Tina wins")        
            break
        franklin.forward(randint(1,5))
        bob.forward(randint(1,5))
        tina.forward(randint(1,5))
        
        
    
